package com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan({"com.service.mapper"})
public class Security2 {
    public static void main(String[] args){
        SpringApplication.run(Security2.class,args);
    }
}
