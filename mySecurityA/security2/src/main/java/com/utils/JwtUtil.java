package com.utils;

import com.sun.javafx.scene.traversal.Algorithm;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

public class JwtUtil {

    private static final String JWT_KEY = "abcd1234";
    private static final Long JWT_TTL = 60*60*1000L;

    public static void main(String[] args){
        System.out.println("Hello Friend!");
        JwtUtil jwtUtil = new JwtUtil();
        String token = jwtUtil.createJwt("mybad");
        System.out.println("Token生成成功--------------\n"+token+"\n开始解析Token------------");
        if(jwtUtil.verifyJwt(token)){
            Claims claims = jwtUtil.parseJwt(token);
            System.out.println("JWT的ID:"+claims.getId());
            System.out.println("被签发人:"+claims.getSubject());
            System.out.println(claims);
        }else {
            System.out.println("Token解析异常！");
        }

    }

    public String createJwt(String subject){
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        Long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        Date expiredDate = new Date(nowMillis+JWT_TTL);
        SecretKey secretKey = getSecretKey();
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        String token = Jwts.builder()
                .setId(uuid)
                .setSubject(subject)
                .setIssuer("root")
                .setIssuedAt(now)
                .signWith(signatureAlgorithm,secretKey)
                .setExpiration(expiredDate)
                .compact();
        return token;
    }

    public boolean verifyJwt(String token){
        try {
            Jwts.parser()
                    .setSigningKey(getSecretKey())
                    .parseClaimsJws(token);
            return true;
        }catch (Exception e){
            System.out.println("验证Token失败："+e.getMessage());
            return false;
        }
    }

    public Claims parseJwt(String token){
        return Jwts.parser()
                .setSigningKey(getSecretKey())
                .parseClaimsJws(token)
                .getBody();
    }

    public SecretKey getSecretKey(){
        byte[] encodedKey = Base64.getDecoder().decode(JWT_KEY);
        return new SecretKeySpec(encodedKey,0,encodedKey.length,"AES");
    }



}
