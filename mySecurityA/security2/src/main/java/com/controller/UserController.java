package com.controller;

import com.domains.Student;
import com.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class UserController {

    @Autowired
    LoginService loginService;

    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }

    @PostMapping("/hey")
    public String hey(Student student){
        return "hey"+student.toString();
    }

    @RequestMapping(value="/noOne",method = RequestMethod.GET,params = {"userName!=NiceBoy","hello"})
    public String noOne(String userName,String hello){
        return hello+",I am "+userName;
    }

    @RequestMapping(value = "/user1/login",method = RequestMethod.POST)
    public String login(){
        return "login Success!";
    }

    @PostMapping("/why")
    public String why(){
        return "This is the reason.";
    }

    @GetMapping("/hey")
    @PreAuthorize("hasAuthority('admin')")
    public String hey(){
        return "H!!!E!!!Y!!!";
    }

    @RequestMapping(value = "/user/hello",method = RequestMethod.POST)
    public String hhh(){
        return "login!!!";
    }

    @RequestMapping(value = "/student/login",method = RequestMethod.POST)
    public Map<String,Object> studentLogin(Student student){
        Map<String ,Object> resultMap = loginService.login(student);
        return resultMap;
    }

    @RequestMapping(value = "/student/logout",method = RequestMethod.GET)
    public Map<String,Object> studentLogout(){
        Map<String ,Object> resultMap = loginService.logout();
        return resultMap;
    }

}
