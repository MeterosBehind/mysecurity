package com.service;

import com.config.LoginStudent;
import com.domains.Student;
import com.service.mapper.MenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    MenuMapper menuMapper;

    @Override
    public UserDetails loadUserByUsername(String userName) {
        /*
        Student student = new Student();
        student.setUserName(userName);
        List<String> permissions = new ArrayList<>();
        if("admin".equals(userName)){
            student.setPassword(passwordEncoder.encode("asd123"));
            student.setId(666123);
            permissions.add("admin");
            permissions.add("root");
            permissions.add("test");
        }else if ("student".equals(userName)){
            student.setPassword(passwordEncoder.encode("asd123"));
            student.setId(666124);
            permissions.add("student");
        }*/
        Student student = menuMapper.selectStudentByUserName(userName);
        student.setPassword(passwordEncoder.encode(student.getPassword()));
        List<String> permissions = menuMapper.selectPermissionsByUid(student.getId());
        List<String> resultPermissions = new ArrayList<>();
        for (String permission:permissions){
            if(resultPermissions.contains(permission)){
                continue;
            }
            for(String tempPer:permission.split(",")){
                if(!resultPermissions.contains(tempPer)){
                    resultPermissions.add(tempPer);
                }
            }
        }

        if(Objects.isNull(student)){
            throw new RuntimeException("用户名或密码错误！");
        }
        return new LoginStudent(student,resultPermissions);
    }

}
