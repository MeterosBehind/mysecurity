package com.service;

import com.config.LoginStudent;
import com.domains.Student;
import com.utils.JwtUtil;
import com.utils.RedisCache;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Service
public class LoginService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    RedisCache redisCache;

    public Map<String,Object> login(Student student){
        Map<String,Object> resultMap = new HashMap();

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(student.getUserName(),student.getPassword());
        try{
            Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
            if(Objects.isNull(authentication)){
                throw new Exception("找不到该用户！");
            }
            Map<String,Object> tempMap = new HashMap();
            LoginStudent loginStudent = (LoginStudent)authentication.getPrincipal();
            String token = new JwtUtil().createJwt(loginStudent.getStudent().getId()+"");
            tempMap.put("token",token);
            student.setId(loginStudent.getStudent().getId());
            student.setPassword(loginStudent.getPassword());
            loginStudent.setStudent(student);
            String studentId = loginStudent.getStudent().getId()+"";
            redisCache.setCacheObject("login:"+studentId,loginStudent);
            student.setId(loginStudent.getStudent().getId());
            student.setPassword("");
            tempMap.put("CurrentStudent",student);
            resultMap.put("state",200);
            resultMap.put("data",tempMap);
            resultMap.put("msg","登录成功！");
        }catch (Exception e){
            System.out.println("登录异常："+e.getMessage());
            resultMap.put("state",500);
            resultMap.put("msg",e.getMessage());
        }
        return resultMap;
    }

    public Map<String ,Object> logout(){
        Map<String,Object> resultMap = new HashMap<>();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            LoginStudent loginStudent = (LoginStudent) authentication.getPrincipal();
            int studentId = loginStudent.getStudent().getId();
            redisCache.deleteObject("login:"+studentId);
            resultMap.put("state",200);
            resultMap.put("msg","登出成功！");
        } catch (Exception e) {
            resultMap.put("state",500);
            resultMap.put("msg","登出异常："+e.getMessage());
            throw new RuntimeException(e);
        }
        return resultMap;
    }
}
