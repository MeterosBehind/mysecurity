package com.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.domains.Menu;
import com.domains.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface MenuMapper extends BaseMapper<Menu> {
    List<String> selectPermissionsByUid(int userId);

    Student selectStudentByUserName(String userName);
}
