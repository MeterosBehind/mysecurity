package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Security3 {
    public static void main(String[] args){
        SpringApplication.run(Security3.class,args);
    }
}
