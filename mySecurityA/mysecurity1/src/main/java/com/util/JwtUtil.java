package com.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

public class JwtUtil {

    //过期时间 1h
    public static final Long JWT_TTL = 60 * 60 * 1000L;
    public static final String JWT_KEY = "abcd1234";

    public static String getUUID(){
        String token = UUID.randomUUID().toString().replaceAll("-","");
        return token;
    }

    public static SecretKey generalKey(){
        byte[] encodedKey = Base64.getDecoder().decode(JwtUtil.JWT_KEY);
        SecretKey secretKey = new SecretKeySpec(encodedKey, 0 ,encodedKey.length, "AES");
        return secretKey;
    }

    public static String createJWT(String subject){
        JwtBuilder builder = getJwtBuilder(subject,null,getUUID());
        return builder.compact();
    }

    public static String createJWT(String id,String subject,Long ttlMillis){
        JwtBuilder builder = getJwtBuilder(subject,ttlMillis,id);
        return builder.compact();
    }

    public static void main(String[] args) throws Exception {
        String subject = "1234admin";
        String jwt = createJWT(subject);
        System.out.println("新生成Token："+jwt);
        String token = jwt;
        Claims claims = parseJWT(token);
        System.out.println("解析\n----------\n过期时间："+claims.getExpiration());
        System.out.println("其他信息："+claims.toString());
    }

    private static JwtBuilder getJwtBuilder(String subject,Long ttlMillis,String uuid){
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        SecretKey secretKey = generalKey();
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        if(ttlMillis == null){
            ttlMillis = JwtUtil.JWT_TTL;
        }
        long expireMillis = nowMillis + ttlMillis;
        Date expireDate = new Date(expireMillis);
        return Jwts.builder()
                .setId(uuid)                                //唯一Id
                .setSubject(subject)                        //主题
                .setIssuer("Abcd1")                         //签发人
                .setIssuedAt(now)                           //签发时间
                .signWith(signatureAlgorithm, secretKey)    //加密方式 密钥
                .setExpiration(expireDate)
                .setNotBefore(now);
    }

    public static Claims parseJWT(String jwt) throws Exception {
//        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        SecretKey secretKey = generalKey();
        return Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(jwt)
                .getBody();
    }
}
