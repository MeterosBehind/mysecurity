package com.controller;

import com.domains.Student;
import com.security.ResponseResult;
import com.service.LoginService;
import com.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class StudentController {
    @Autowired
    LoginService loginService;

    public static void main(String []args){
        Student student = new Student();
        student.setName("123");
        System.out.println(student);
        System.out.println(UUID.randomUUID());
        String a = "及";
        char[] chars = a.toCharArray();
        int asc1 = chars[0];
        System.out.println(asc1);
    }

    @RequestMapping("/hello")
    public String Hello(){
        return "Hello";
    }

    @RequestMapping("/user/login")
    public ResponseResult login(User user) throws Exception {
        return loginService.login(user);
    }

    @GetMapping("/getImportantInfo")
    public String getInfo(){
        return "This is the very important information!";
    }

    @GetMapping("/user/logout")
    public ResponseResult logout() throws Exception {
        return loginService.logout();
    }
}
