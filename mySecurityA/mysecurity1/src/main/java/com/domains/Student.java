package com.domains;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class Student {
    private String name;
    private String sex;
    private int age;
    private int height;
    private int weight;
}
