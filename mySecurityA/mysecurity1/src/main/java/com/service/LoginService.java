package com.service;

import com.controller.User;
import com.security.ResponseResult;

public interface LoginService {
    public ResponseResult login(User user) throws Exception;
    public ResponseResult logout() throws Exception;
}
