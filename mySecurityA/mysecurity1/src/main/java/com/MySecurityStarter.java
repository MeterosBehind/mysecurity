package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;

@SpringBootApplication
public class MySecurityStarter {
    public static void main(String[] args){
        SpringApplication.run(MySecurityStarter.class,args);
    }
}
